#!/usr/bin/python
# -*- coding: utf-8 -*-

# 導入: python 新版本特性
from __future__ import unicode_literals
from __future__ import absolute_import
from __future__ import print_function
from __future__ import division

# 載入: 標準函式庫模組
import sys
import os
import time

# 載入: 自定函式庫
sys.path.append(os.path.dirname(os.path.abspath(__file__)) + b"/tool")

from backup import Backup


if __name__ == "__main__":
    # 定義: 自定義變數
    MYSQL_BK_DIR = "/mnt/nas/backup/db/"    # MySQL備份檔存放位置
    TMP_ROOT_DIR = "/tmp/"  # 暫存檔存放之目錄
    DB_HOST = "localhost"   # 資料庫 ip
    DB_USER = "root"    # 資料庫備份之帳號
    DB_PASSWD = "123456"    # 資料庫備份之密碼
    BACKUP_DB = ["mysql"]  # 欲備份之資料庫(list 資料型態)

    try:
        # 建立: 暫存目錄
        date = time.strftime("%Y%m%d")
        tmp_path = os.path.join(TMP_ROOT_DIR, date)

        if not os.path.isdir(tmp_path) :
            os.mkdir(tmp_path)

        # 備份: mysql
        backup = Backup()
        backup.mysqldump(DB_USER, DB_PASSWD, host=DB_HOST, db=BACKUP_DB, dist_dir=tmp_path)

        # 壓縮: mysql備份檔
        compressed_file = os.path.join(MYSQL_BK_DIR, "mysql-" + date + ".tgz")
        backup.compress(tmp_path, compressed_file)

        # 刪除: 暫存檔&暫存目錄
        backup.rm_expired_children(tmp_path, 0)
        os.rmdir(tmp_path)

        # 刪除: 過期備份檔
        backup.rm_expired_children(MYSQL_BK_DIR, 7)
    except Exception as err:
        # 輸出: 錯誤訊息
        print(err)
        sys.exit(1)