#!/usr/bin/python
# -*- coding: utf-8 -*-

# 導入: python 新版本特性
from __future__ import unicode_literals
from __future__ import absolute_import
from __future__ import print_function
from __future__ import division

# 載入: 標準函式庫模組
import traceback
import distutils.spawn
import time
import os
import subprocess
import tarfile
import sys
import shutil


class Backup(object):
    """備份工具
    
    一些在備份時會用的到相關功能
    
    Attributes:
        __switch: 私有的dictionary，用於標示某些特殊功能是否啟用。
    """

    def __init__(self):
        """初始化: class屬性變數"""
        # 初始化: super class
        super(Backup, self).__init__()

        # 初始化: class 屬性變數
        self.__switch = {"debug":False}


    def mysqldump(self, account="", password="", host="", db=[], dist_dir = "./"):
        """備份: MySQL資料
        
        以系統指令 mysqldump 備份MySQL資料庫資料到.sql檔案，若有指定備份的資料庫則會分別將資料
        檔案備份至 {db_name}-{備份日期}.sql 中，若未指定備份的資料庫則會備份整個 MySQL 資料到
        mysql-{備份日期}.sql 中
        
        Args:
            account: 登入之帳號
            password: 登入之密碼
            host: 愈備份之資料庫 ip
            db: 要備份的資料庫列表，若為空的 list 則直接備份整個 MySQL
            dist_dir: 備份出來的.sql檔案存放的目錄

        Returns:
            result: boolean值，True為備份成功，False則為備份失敗
            
        Raises:
            SystemError: mysqldump 指令不存在或執行失敗...等錯誤
        """
        try:
            # 檢查: 參數
            if account == "":
                raise ValueError("參數 account 不能為空")

            # 檢查: mysqldump 是否存在
            cmd = distutils.spawn.find_executable("mysqldump")

            if cmd is None:
                raise SystemError("系統指令 mysqldump 不存在")

            # 串接: 共用指令字串
            cmd += " -u" + account
            cmd += (" -p" + password) if password != "" else ""
            cmd += (" -h " + host) if host != "" else ""

            # 串接 & 執行: 指令字串
            if not db:
                # 設置: 備份檔案
                file = "mysql-" + time.strftime("%Y%m%d") + ".sql"
                file = os.path.join(dist_dir, file)

                # 執行: 指令
                _cmd = cmd + " --all-databases > " + file
                result = subprocess.call(_cmd, shell=True)

                # 檢查: 執行結果
                if result != 0:
                    raise SystemError(exec_cmd + " 執行失敗")
            else:
                for _db in db:
                    # 設置: 備份檔案
                    file = _db + "-" + time.strftime("%Y%m%d") + ".sql"
                    file = os.path.join(dist_dir, file)

                    # 執行: 指令
                    _cmd = cmd + " --databases " + _db + " > " + file
                    result = subprocess.call(_cmd, shell=True)

                    # 檢查: 執行結果
                    if result != 0:
                        raise SystemError(_cmd + " 執行失敗")

            # 設置: 結果
            result = True
        except:
            self.__debug()
            result = False

        return (result)


    def compress(self, src, dest, excludes=[]):
        """壓縮: 檔案/目錄
        
        壓縮特定的目錄或檔案到指定的路徑下，可指定要排出壓縮的目錄/檔案
        
        Args:
            src: 欲壓縮的目錄/檔案
            dest: 壓縮檔的路徑
            excludes: 排除壓縮的目錄/檔案列表，為相對路徑並以要壓縮的目錄/檔案之上層目錄為基準，
                      其中，排除之目錄的路徑不能以"/"結尾，且其底下之子目錄與檔案將被一併排除

        Returns:
            result: boolean值，True為備份成功，False則為備份失敗
            
        Raises:
            RuntimeError: 執行過程中的環境不正確
        """
        try:
            # 去除: 無用之空白字元
            dest = dest.strip()
            src = src.strip()

            # 檢查: 指定的壓縮檔路徑
            if os.path.isdir(dest) == True:
                raise RuntimeError("產生的壓縮檔路徑為目錄")
            elif os.path.isdir(os.path.dirname(dest)) == False:
                raise RuntimeError("產生的壓縮檔路徑錯誤")

            # 檢查: 來源是否為目錄
            if os.path.isdir(src) == True:
                # 去除: 末端之目錄分隔符號
                src = src.rstrip(os.sep)
            elif os.path.isfile(src) == False :
                raise RuntimeError("欲壓縮的檔案或目錄不存在")

            # 定義: arcname
            arcname = os.path.split(src)
            arcname = arcname[1]

            # 建立: 壓縮檔
            tar = tarfile.open(dest, "w:gz")
            if sys.version_info[:2] >= (2, 7):
                # 檢查: python版本 >= 2.7，使用 filter 參數
                tar.add(src, arcname=arcname, filter=lambda file: None if file.name in excludes else file)
            elif sys.version_info[:2] == (2,6):
                # 檢查: python版本 = 2.6，使用 exclude 參數
                tar.add(src, arcname=arcname, exclude=lambda file: os.path.relpath(file, os.path.split(src)[0]) in excludes)
            tar.close()

            # 設置: 結果
            result = True
        except:
            self.__debug()
            result = False

        # 回傳: 結果
        return (result)


    def rm_expired_children(self, target, days=0):
        """刪除: 過期目錄/檔案
        
        刪除指定目錄下一層所有修改日期以執行時間開始計算超過設定的存活天數之檔案或目錄
        
        Args:
            target: 要進行刪除動作的目錄路徑
            day: 指定可存活的天數

        Returns:
            result: boolean值，True為備份成功，False則為備份失敗
            
        Raises:
            RuntimeError: 執行過程中的環境不正確
        """
        try:
            # 檢查: 目錄是否存在
            if os.path.isdir(target) is False:
                raise RuntimeError("目標目錄不存在")

            # 取得: 目錄下層列表
            list = os.listdir(target)

            # 檢查: 修改時間
            expired = days * 24 * 60 * 60
            now = time.time()

            for item in list:
                aim = os.path.join(target, item)
                mtime = os.path.getmtime(aim)

                # 刪除: 檔案|目錄
                if (now - mtime) >= expired:
                    if os.path.isdir(aim) == True:
                        shutil.rmtree(aim)
                    elif os.path.isfile(aim) == True:
                        os.remove(aim)

            # 設置: 結果
            result = True
        except:
            self.__debug()
            result = False

        # 回傳: 結果
        return (result)


    def set_debug(self, on=False):
        """設置: debug模式開關"""
        # 設置: debug 開關
        self.__switch["debug"] = on


    def __debug(self):
        """顯示: 除錯訊息"""
        # 顯示: 錯誤訊息
        if (self.__switch["debug"] == True):
            print(traceback.format_exc())

'''
if __name__ == "__main__":
    # 定義: 變數
    backup_tool = Backup()

    # 啟動: debug 模式
    backup_tool.set_debug(True)

    # 備份: 資料庫
    backup_tool.mysqldump("root", "123456", "127.0.0.1", ["goodilov_db"])

    # 壓縮:
    backup_tool.compress("/tmp/test/", "/tmp/test_tmp/test.tgz", excludes=["test/test_dir"])   # 目錄不能以 / 結尾

    # 刪除:
    backup_tool.rm_expired("./", 3)
'''

